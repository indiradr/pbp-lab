// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:lab_6/screens/stotes_home.dart';
import 'package:lab_6/screens/searchProjects.dart';

class pageRoutes {
  static const String home = StotesHome.routeName;
  static const String search = SearchProjects.routeName;
}
