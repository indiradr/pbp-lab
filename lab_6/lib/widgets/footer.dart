import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  const Footer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      color: const Color(0xFF666c4e),
      width: double.infinity,
      height: 50.0,
      child: Column(
        children: const <Widget> [
          SizedBox(height: 15),
          Text("mVag©2021",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18,
                      color: Color(0xFFFDF8EE),
                      fontWeight: FontWeight.bold),),
          ],
        )
    );
  }
}