import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselTaskCard extends StatefulWidget {
  const CarouselTaskCard({ Key? key }) : super(key: key);

  @override
  _CarouselTaskCardState createState() => _CarouselTaskCardState();
}

class _CarouselTaskCardState extends State<CarouselTaskCard> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(height: 150.0),
      items: ["Essay in Sappho",
              "Design in Main Character",
              "Report in Rome"
        ].map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.all(5.0),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color(0xFFebaa86),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x201a1e24),
                      spreadRadius: 1,
                      blurRadius: 4,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: FittedBox(
                        child: Text(i, 
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                        fontSize: 20,
                        color: Color(0xFF523115),
                        fontWeight: FontWeight.bold,
                      ),
                      ),
                      ),
                    ),
                  ],
                ),
            );
          },
        );
      }).toList(),
    );
  }
}