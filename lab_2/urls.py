from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name="HTML"),
    path('xml', xml, name="XML"),
    path('json', json, name="JSON")
]