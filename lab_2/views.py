from django.http import response
from django.shortcuts import render
from .models import Note
from django.http import HttpResponse
from django.core import serializers


# Create index method
def index(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab2.html', response)

# create xml method
def xml(request):
    notes = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(notes, content_type="application/xml")

def json(request):
    notes = serializers.serialize('json', Note.objects.all())
    return HttpResponse(notes, content_type="application/json")