from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    from_who = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.TextField()
    song_link = models.URLField(default="https://open.spotify.com/track/4cOdK2wGLETKBW3PvgPWqT?si=07436adf12e04161")
    album_cover = models.URLField(default="https://64.media.tumblr.com/f9552fa08245ab7fdeda32fa2715b92c/d32c8f001a4e3315-72/s1280x1920/ba54bc79448252f123350e53417ecbacdd9f1d94.jpg")