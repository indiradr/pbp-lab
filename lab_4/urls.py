from django.urls import path
from .views import index, add_note, note_list
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name="card"),
    path('add-note', add_note, name="add_note"),
    path('note-list', note_list, name="note_list")

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
