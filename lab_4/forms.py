from django.db.models import fields
from django.db import models
from django.forms import ModelForm
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:         # add Meta class
        model = Note
        fields = '__all__'    # set fields