from django.http import response
from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm



def index(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    response = {}
    
    form = NoteForm(request.POST or None) 

    # check if request data is POST
    if request.method == "POST" and form.is_valid():
        # save the form data to model
        form.save()
        return redirect('/lab-4/') # redirect to table that shows friends

    response['form'] = form

    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_note_list.html', response)