import 'package:flutter/material.dart';
import 'package:lab_7/screens/edit_profile.dart';
import 'package:lab_7/screens/stotes_home.dart';
import 'package:lab_7/screens/searchProjects.dart';
import 'package:lab_7/routes/pageRoutes.dart';

void main() {
  runApp(const MyApp());
}

Map<int, Color> color = {
  50: Color.fromRGBO(102, 108, 78, .1),
  100: Color.fromRGBO(102, 108, 78, .2),
  200: Color.fromRGBO(102, 108, 78, .3),
  300: Color.fromRGBO(102, 108, 78, .4),
  400: Color.fromRGBO(102, 108, 78, .5),
  500: Color.fromRGBO(102, 108, 78, .6),
  600: Color.fromRGBO(102, 108, 78, .7),
  700: Color.fromRGBO(102, 108, 78, .8),
  800: Color.fromRGBO(102, 108, 78, .9),
  900: Color.fromRGBO(102, 108, 78, 1),
};

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xFF666C4E, color);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stotes',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: colorCustom,
        canvasColor: const Color(0xFFFDF8EE),
      ),
      home: const MyHomePage(title: 'Stotes'),
      routes: {
        pageRoutes.home: (context) => const StotesHome(),
        pageRoutes.search: (context) => const SearchProjects(),
        pageRoutes.profile: (context) => const Profile(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return StotesHome();
  }
}
