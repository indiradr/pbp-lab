import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselProjectCard extends StatefulWidget {
  const CarouselProjectCard({Key? key}) : super(key: key);

  @override
  _CarouselProjectCardState createState() =>  _CarouselProjectCardState();
}

class _CarouselProjectCardState extends State<CarouselProjectCard> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(height: 150.0),
      items: ["Midterm Project",
              "The Amazing Magician: Storyboard",
              "Database Group Assignment"
        ].map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.all(5.0),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color(0xFFb5be8e),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x201a1e24),
                      spreadRadius: 1,
                      blurRadius: 4,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                    ],
                ),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: FittedBox(
                        child: Text(i, 
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                        fontSize: 24,
                        color: Color(0xFF523115),
                        fontWeight: FontWeight.bold,
                      ),
                      ),
                      ),
                    ),
                  ],
                ),
            );
          },
        );
      }).toList(),
    );
  }
}
