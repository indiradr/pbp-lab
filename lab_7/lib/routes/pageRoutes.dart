// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:lab_7/screens/stotes_home.dart';
import 'package:lab_7/screens/searchProjects.dart';
import 'package:lab_7/screens/edit_profile.dart';

class pageRoutes {
  static const String home = StotesHome.routeName;
  static const String search = SearchProjects.routeName;
  static const String profile = Profile.routeName;
}
