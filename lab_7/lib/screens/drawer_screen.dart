import 'package:flutter/material.dart';
import 'package:lab_7/routes/pageRoutes.dart';
import 'package:lab_7/screens/stotes_home.dart';


class DrawerScreen extends StatefulWidget {
  const DrawerScreen({ Key? key }) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text("indira.devi"),
            accountEmail: const Text("indiradevirr@gmail.com"),
            currentAccountPicture: Container(
            margin: const EdgeInsets.only(top:10.0, bottom: 10.0),
            width: 200.0,
            height: 200.0,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('/Users/rusvandyrously/Documents/code/PBP/pbp-lab/lab_6/assets/images/default_pp.png')
              ),
            ),
          ),
          ),

          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () => Navigator.pushReplacementNamed(context, pageRoutes.home)
          ),

          DrawerListTile(
            iconData : Icons.account_circle_sharp,
            title: "My profile",
            onTilePressed: () => Navigator.pushReplacementNamed(context, pageRoutes.profile)
          ),

          
          DrawerListTile(
            iconData: Icons.folder_rounded,
            title: "My Projects",
            onTilePressed: () {
            },
          ),

          DrawerListTile(
            iconData: Icons.search,
            title: "Search Projects",
            onTilePressed: () => Navigator.pushReplacementNamed(context, pageRoutes.search)
          ),

          DrawerListTile(
            iconData: Icons.group,
            title: "Meetings",
            onTilePressed: () {
            },
          ),

          DrawerListTile(
            iconData: Icons.door_back_door,
            title: "Logout",
            onTilePressed: () {
            },
          ),
          
        ],
      ),
    );

  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData ;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: const TextStyle(fontSize: 16),),
    );

  }
}