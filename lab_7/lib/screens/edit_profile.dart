// ignore_for_file: file_names, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/screens/drawer_screen.dart';
import 'package:lab_7/widgets/footer.dart';



class Profile extends StatefulWidget {
  const Profile({ Key? key }) : super(key: key);
  static const String routeName = 'profile';
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text(
          "indira.devi's Profile",
          textAlign: TextAlign.left,
          style: TextStyle(
              fontSize: 32, fontFamily: "Sunflower", color: Color(0xFF341E0B)),
        ),
        centerTitle: false,
      ),
      drawer: const DrawerScreen(),
      bottomSheet: const Footer(),
      body: SingleChildScrollView(
        child: Center(
        child: Column(
              children: <Widget>[
              const SizedBox(height: 20,),
              Image.asset('/images/default_pp.png',
                width: 80,
                height: 80,),  
              const SizedBox(height: 20,),
              const Text("Username : indira.devi",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: "Sunflower",
                          color: Color(0xFF523115))),
              const SizedBox(height: 20,),
              const Text("Email : indiradevirr@gmail.com",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: "Sunflower",
                          color: Color(0xFF523115))),
              const SizedBox(height: 20,),
              Padding(padding: const EdgeInsets.all(20.0),
              child: TextFormField(
                decoration: InputDecoration(
                    hintText: "Say hello!",
                    contentPadding: const EdgeInsets.fromLTRB(12, 20, 0, 0),
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(top: 15), // add padding to adjust icon
                      child: Icon(Icons.search),
                  ),
                    
                ),
              ),
              ),

              SizedBox(height: 12,),
              ElevatedButton(
                style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                  padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),),
                onPressed: () {
                  setState(() {
                  pressed = true;
              });
              },
                child: const Text('Submit'),
              ),
              SizedBox(height: 20,),
              pressed ? Text("Hello, Indira!") : SizedBox(height: 40,),
              
          ],
        )
    )
    )
    
    );
    
  }
}