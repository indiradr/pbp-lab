// ignore_for_file: file_names, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/screens/drawer_screen.dart';
import 'package:lab_7/widgets/footer.dart';



class SearchProjects extends StatefulWidget {
  const SearchProjects({ Key? key }) : super(key: key);
  static const String routeName = 'searchProjects';
  
  @override
  _SearchProjectsState createState() => _SearchProjectsState();
}

class _SearchProjectsState extends State<SearchProjects> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text(
          "Stotes",
          textAlign: TextAlign.left,
          style: TextStyle(
              fontSize: 40, fontFamily: "Sunflower", color: Color(0xFF341E0B)),
        ),
        centerTitle: false,
      ),
      drawer: const DrawerScreen(),
      bottomSheet: const Footer(),
      body: SingleChildScrollView(
        child: Center(
        child: Column(
              children: <Widget>[
              const SizedBox(height: 20,),
              const Text("Look Up Projects",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 40,
                          fontFamily: "Sunflower",
                          color: Color(0xFF523115))),
              const SizedBox(height: 20,),
              Padding(padding: const EdgeInsets.all(20.0),
              child: TextFormField(
                
                decoration: InputDecoration(
                    hintText: "Enter project name...",
                    contentPadding: const EdgeInsets.fromLTRB(12, 20, 0, 0),
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(top: 15), // add padding to adjust icon
                      child: Icon(Icons.search),
                  ),
                    
                ),
              ),
              ),

              SizedBox(height: 12,),
              ElevatedButton(
                style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                  padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),),
                onPressed: () {},
                child: const Text('Search'),
              ),
              SizedBox(height: 40,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 150,
                margin: const EdgeInsets.all(5.0),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color(0xFFb5be8e),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x201a1e24),
                      spreadRadius: 1,
                      blurRadius: 4,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Stack(
                  children: const [
                    Align(
                      alignment: Alignment.center,
                      child: FittedBox(
                        child: Text("Test", 
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                        fontSize: 20,
                        color: Color(0xFF523115),
                        fontWeight: FontWeight.bold,
                      ),
                      ),
                      ),
                    ),
                  ],
                ),
            )
          ],
        )
    )
    )
    );
        
  }
}