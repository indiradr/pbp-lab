import 'package:flutter/material.dart';
import 'package:lab_7/widgets/carousel_project_card.dart';
import 'package:lab_7/widgets/carousel_taskcard.dart';
import 'package:lab_7/screens/drawer_screen.dart';
import 'package:lab_7/widgets/footer.dart';

class StotesHome extends StatefulWidget {
  const StotesHome({Key? key}) : super(key: key);
  static const String routeName = '';

  @override
  _StotesHomeState createState() => _StotesHomeState();
}

class _StotesHomeState extends State<StotesHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text(
          "Stotes",
          textAlign: TextAlign.left,
          style: TextStyle(
              fontSize: 40, fontFamily: "Sunflower", color: Color(0xFF341E0B)),
        ),
        centerTitle: false,
      ),
      drawer: const DrawerScreen(),
      bottomSheet: const Footer(),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: Container(
              color: const Color(0xFFFDF8EE),
              child: Column(
                children: const [
                  SizedBox(height: 24),
                  Text("Hello, indira.devi!",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 40,
                          fontFamily: "Sunflower",
                          color: Color(0xFF523115))),
                  SizedBox(height: 24),
                  Text("Recent Projects",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: Color(0xFF523115))),
                  SizedBox(height: 10),
                  CarouselProjectCard(),
                  SizedBox(height: 24),
                  Text("Upcoming Tasks",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: Color(0xFF523115))),
                  SizedBox(height: 10),
                  CarouselTaskCard(),
                  SizedBox(height: 100),
                ],
              )
              ),
            ),
        ]
        ),
    )
    );
  }
}
