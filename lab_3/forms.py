from django.db.models import fields
from django.db import models
from django.forms import ModelForm
from lab_1.models import Friend

class FriendForm(ModelForm):
    class Meta:         # add Meta class
        model = Friend
        fields = ['name', 'npm', 'dob']     # set fields