from .forms import FriendForm
from django.shortcuts import redirect, render
from django.urls import reverse
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# decorator so if we for example go incognito we have to sign in first as admin
@login_required(login_url='/admin/login')       

def index(request):
    friends = Friend.objects.all() # save friend objects 
    response = {'friends': friends} 
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    response = {}
    
    form = FriendForm(request.POST or None) 

    # check if request data is POST
    if request.method == "POST" and form.is_valid():
        # save the form data to model
        form.save()
        return redirect('/lab-3/') # redirect to table that shows friends

    response['form'] = form

    return render(request, 'lab3_form.html', response)