from django.db import models

# Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    # Implement attributes in Friend model
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    dob = models.DateField()
    
