
1. Difference between JSON and XML

JSON—also known as JavaScript Object Notation, is a data-interchange format that's language-independent but is based on JavaScript, so a JavaScript program can easily convert JSON data into JavaScript objects. On the other hand, XML or Extensible Markup Language is a markup language that can help add information to free-flowing text that is both human and machine-readable. JSON is used to transmit data in a parseable manner via the internet. When storing, the data has to be in a specific format, and regardless of where to keep it, text is always one of the legal formats. In comparison, XML helps us structure data, which the user can annotate Metadata or parse scripts.  

2. Difference between XML and HTML

Even though the syntax of HTML and XML are pretty similar, these two markup languages have different purposes.Firstly, markup languages key feature is how tags are used in defining the page layout and the elements within the page. The application of HTML (Hypertext Markup Language) is to displays data and describes a web page’s structure. An HTML file is used to create websites that can be viewed by anyone on the internet. The tags mentioned before in markup languages are the words between the "< >" and separate standard text from HTML code. However, he tags are not displayed on the webpages but affect the appearance of data on webpages, and a closing tag (</>) is not always necessary in HTML.

XML is another markup language, but the key feature of it is to encode documents, that are defined by a set of rules and in a format that is both human and machine readable. The tags (< >) in an XML document is used to define the document structure, how it should be stored and transported. Unlike HTML, closing tags (</>) in XML are mandatory. The language focuses on being used to represent data structures in web services. XML can also be used in any system, as it's a platform-independent programming language. 
